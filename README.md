# GitLab Caching For Local Maven Repository
## Proof of Concept
## Summary
Maven stores dependencies in a local repo. Maven will search this local repo before downloading from a central repo (the slow step we want to remove)
Before, the local repo may not have been preserved across pipeline runs. There is a maven cli option to set the path of where the local repo is, so that we can use gitlab’s caching feature to preserve the repo between runs. 
This approach in the proof of concept saves about 12 seconds using the cache. See the [pom file](https://gitlab.com/m1612/cognistuff/maven-cache-test/-/blob/main/pom.xml) of the repo to see what dependencies were used and saved.

## Reference Links:
- [Gitlab docs about caching](https://docs.gitlab.com/ee/ci/caching/)
    - Has examples about nodeJS dependencies and python dependencies
    - Has info about best practices to make caching efficient
    - [Alternative](https://docs.gitlab.com/12.10/ee/ci/caching/)
- [Maven caching example](https://www.google.com/url?q=https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Maven.gitlab-ci.yml&sa=D&source=editors&ust=1629999063996000&usg=AOvVaw3RzADRnd4tbmb2CM9TruSw) from GitLab templates
    - Is more complicated and has extra options to explore


Without Caching: (47 seconds)
[Pipeline Run](https://gitlab.com/m1612/cognistuff/maven-cache-test/-/jobs/1538210244#L3): 1 min 23 secs
![image.png](./image.png)

With Caching: (24 seconds + 5[restore cache] + 6[save cache] = 35)
[Pipeline Run](https://gitlab.com/m1612/cognistuff/maven-cache-test/-/jobs/1538218775): 1 min 13 secs
![image-1.png](./image-1.png)

## Conclusions:
Time saves will depend on the number of dependencies, since this was a smaller Spring Boot application. In this example, we saved 12 seconds per run.

Overall runs went from about 1:30 to 1:10, variability probably came from using shared runners. (see pipeline history link)

With the GitLab Docker Executor (I assume this is what pipelines use), caches are volumes stored on the runner in a specific directory.
The gitlab cache documentation has info on [best practices](https://docs.gitlab.com/ee/ci/caching/#good-caching-practices) and how to make it efficient.

Based on the tests, this does not seem to be necessary, but there is an [“offline” flag](https://www.baeldung.com/maven-offline) in Maven that would force maven to never look at online repos, but this will break if there is a new dependency not in the local repo. Extra fallbacks will be needed in this case.

Self-provisioned runners may not have the 5 second restore cache step (unconfirmed, needs experimentation)
